import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GLOBAL } from './GLOBAL';
import { Observable, observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ColaboradorService {

  public url: any = GLOBAL.url;

  constructor(
    private _http: HttpClient
  ) {
    console.log(this.url)
  }

  login_admin(data: any): Observable<any> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
    return this._http.post(this.url+'login_admin',data,{headers:headers})
  }

  registro_colaborador_admin(data: any): Observable<any> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
    return this._http.post(this.url+'registro_colaborador_admin',data,{headers:headers})
  }


}
