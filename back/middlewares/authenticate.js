var jwr = require('jwt-simple')
var moment = require('moment')
var secret = 'djarson'

exports.auth = function(req, res, next) {
    if (!req.headers.authorization) {
        return res.status(403).send({ message: 'NoHeadersError' });
    }

    var token = req.headers.authorization.replace(/['"]+/g, '');
    //HEADER 
    //PAYLOD          ESTAN SON LAS 3 PARTES QUE DIVIDEN AL TOKEN
    //FIRMA

    var segment = token.split('.'); //LAS SEPARAMOS POR SUS PARTES

    if (segment.length != 3) { //SI EL SEGMENTO ES MAYOR A 3 PARTES O DIFERENCTE DE 3
        return res.status(403).send({ message: 'InvalidToken' });
    } else {
        try {
            var payload = jwt.decode(token, secret);
            if (payload.exp <= moment().unix()) {
                return res.status(403).send({ message: 'TokenExpirado' });
            }
        } catch (error) {
            console.log(error);
            return res.status(403).send({ message: 'ErrorToken' });
        }
    }

    req.user = payload;

    next();
}