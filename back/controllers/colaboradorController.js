var Colaborador = require('../models/Colaborador')
var bcrypt = require('bcrypt-nodejs')
var jwt = require('../helpers/jwt')

const registro_colaborador_admin = async function(req, res) {
    // console.log(req) //para imprimir el requerimiento q hicimos de registrar
    let data = req.body; //para imprimir solo los datos del formulario
    // let colaborador = await Colaborador.create(data);
    // res.status(200).send({data: colaborador});
    if (req.user) {
        try {
            var colaboradores = await Colaborador.find({ email: data.email })
            bcrypt.hash('123456', null, null, async function(err, hash) {
                if (err) {
                    res.status(200).send({ data: undefined, message: 'no se puedo guardar la contrasena' });
                } else {
                    if (colaboradores.length >= 1) {
                        res.status(200).send({ data: undefined, message: 'el correo ya existe' });
                    } else {
                        data.fullnames = data.nombres + '' + data.apellidos;
                        data.password = hash;
                        let colaborador = await Colaborador.create(data);
                        res.status(200).send({ data: colaborador });
                    }

                }
            })

        } catch (error) {
            res.status(200).send({ data: undefined, message: 'verifique los campos del formulario' });
        }
    } else {
        res.status(403).send({ data: undefined, message: 'noToken' });
    }
}

const login_admin = async function(req, res) {
    let data = req.body;
    var colaboradores = await Colaborador.find({ email: data.email })
    if (colaboradores.length >= 1) {
        console.log(colaboradores[0].estado)
        if (colaboradores[0].estado) {
            bcrypt.compare(data.password, colaboradores[0].password, async function(err, check) {
                if (check) {
                    res.status(200).send({ data: colaboradores[0], token: jwt.createToken(colaboradores[0]) });
                } else {
                    res.status(200).send({ data: undefined, message: 'la contrasena es incorrecta' });
                }
            });
        } else {
            res.status(200).send({ data: undefined, message: 'ya no tienes acceso' });
        }
    } else {
        res.status(200).send({ data: undefined, message: 'el correo  no existe' });

    }
}
module.exports = {
    registro_colaborador_admin,
    login_admin
}